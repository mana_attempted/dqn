import keras
from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense, Flatten
from keras.optimizers import Adam

import numpy as np
import random
from collections import deque

from functions import *

import json

configs = json.load(open('config.json', 'r'))

class Agent:
	def __init__(self, state_size, is_eval=False, model_name=""):
		self.state_size = state_size # normalized previous days
		self.action_size = 3 # sit, buy, sell
		self.memory = deque(maxlen=1000)
		self.model_name = model_name
		self.is_eval = is_eval

		self.gamma = 0.1
		# self.gamma = 1
		self.epsilon = 1.0
		self.epsilon_min = 0.01
		self.epsilon_decay = 0.9999

		self.batch_size = 128

		self.count_gym_stat_to_train = 6

		self.model = load_model("models/" + model_name) if is_eval else self._model()

		if is_eval:
			print("load weight from model", model_name)
		else:
			print("new model")

	def _model(self):
		model = Sequential()
		model.add(Dense(units=64, input_shape=(self.state_size, len(configs['data']['columns'])+self.count_gym_stat_to_train), activation="relu"))
		# model.add(Dense(units=64, input_dim=self.state_size, activation="relu"))
		model.add(Dense(units=64, activation="relu"))
		model.add(Dense(units=64, activation="relu"))
		model.add(Dense(units=64, activation="relu"))
		model.add(Dense(units=64, activation="relu"))
		# model.add(Dense(units=32, activation="relu"))
		# model.add(Dense(units=8, activation="relu"))
		model.add(Flatten())
		model.add(Dense(self.action_size, activation="linear"))
		model.compile(loss="mse", optimizer=Adam(lr=0.001))

		return model

	def act(self, state):
		if not self.is_eval and random.random() <= self.epsilon:
			return random.randrange(self.action_size)

		options = self.model.predict(state)
		return np.argmax(options[0])

	def actTrain(self, state, price, sma200):

		if not self.is_eval and random.random() <= self.epsilon:
		
			if price > sma200:
				return 1
			elif price < sma200:
				return 2
			else:
				return 0

		options = self.model.predict(state)
		return np.argmax(options[0])

	def expReplay(self, batch_size):
		mini_batch = []
		l = len(self.memory)
		for i in range(l - batch_size + 1, l):
			mini_batch.append(self.memory.popleft())
		states, targets_f = [], []
		for state, action, reward, next_state, done in mini_batch:
			target = reward
			if not done:
				# print("next_state:", next_state)
				# print("next_state.shape:", next_state.shape)
				target = reward + self.gamma * np.amax(self.model.predict(next_state)[0])

			# print("state:", state)
			# print("state.shape:", state.shape)

			target_f = self.model.predict(state)

			# print("target_f:", target_f)
			# print("target_f.shape:", target_f.shape)
			# print("target:", target)
			# if action >= self.action_size:
			# 	print("action:", action)

			
			# print("target_f[0][action]:", target_f[0][action])
			# print("target_f[0]["+str(int(action/3))+"]"+"["+str(int(action%3))+"]")
			# print(str(target_f[0][int(action/3)][int(action%3)]))
			target_f[0][action] = target
			# target_f[0][int(action/self.action_size)][int(action%self.action_size)] = target
			# target_f[0][0][int(action%self.action_size)] = target
			states.append(state[0])
			targets_f.append(target_f[0])
			# self.model.fit(state, target_f, epochs=1, verbose=0)
		self.model.fit(np.array(states), np.array(targets_f), epochs=1, verbose=0)

		if self.epsilon > self.epsilon_min:
			self.epsilon *= self.epsilon_decay