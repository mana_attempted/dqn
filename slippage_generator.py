import pandas as pd
import numpy as np
import sys, os

stock_name = sys.argv[1]

# df = pd.read_csv("data/" + stock_name + ".csv", parse_dates=['Date'], index_col='Date')

EPISODE = 10000
SEPERATOR = 500

for x in range(EPISODE+1):

    turn = x+1

    print("gen: "+str(turn)+"/"+str(EPISODE))

    df = pd.read_csv("data/" + stock_name + ".csv", parse_dates=['Date'], index_col='Date')

    # if turn > SEPERATOR:
    #     file_number = str( (int( (turn/SEPERATOR)-1 ) *SEPERATOR ) + turn%SEPERATOR )
    #     df = pd.read_csv("data/slippage/"+stock_name+"/" +file_number+ ".csv", parse_dates=['Date'], index_col='Date')
    # else:
    #     df = pd.read_csv("data/" + stock_name + ".csv", parse_dates=['Date'], index_col='Date')

    slippages = (100+np.random.uniform(low=-1, high=1, size=df.shape[0]))/100

    df['Slippage'] = slippages
    df['Close'] = df['Close'].mul(slippages, axis=0)

    path = "data/slippage/"+stock_name+"/"
    file_name = str(turn)+".csv"

    final_path = path+file_name

    if not os.path.isdir(path):
        os.makedirs(path)

    df.to_csv(final_path)

print("done.")