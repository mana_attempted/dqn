import numpy as np
import math
import pandas as pd
from scipy.stats import linregress

# prints formatted price
def formatPrice(n):
	return ("-" if n < 0 else "") + "{0:,.2f}".format(abs(n))

def formatPercent(n):
	return "{0:,.2f}".format(n)+"%"

def formatInteger(n):
	return "{0:,.0f}".format(round(n))

def calPercent(source, consider):
	return (consider/source)*100

# returns the vector containing stock data from a fixed file
def getStockDataVec(key, key_week, cols, price_multiplier=1):
	
	# vec = []
	# lines = open("data/" + key + ".csv", "r").read().splitlines()

	# for line in lines[1:]:
	# 	try:
	# 		vec.append(float(line.split(",")[4]))
	# 	except Exception as e:
	# 		print('.')

	# # return np.array(vec), np.array(vec)
	# return vec, vec

	dataframe = pd.read_csv("data/" + key + ".csv", parse_dates=['Date'])
	dataframe.dropna(axis=0, how='any', inplace=True)#drop null row
	dataframe = dataframe[(dataframe != 0).all(1)]#drop 0 row
	dataframe = dataframe.drop_duplicates(subset=['Date'], keep='last')

	data = (dataframe.get(['Close']).values)*price_multiplier

	feature = (dataframe.get(cols).values)*price_multiplier

	sma200 = calSMA(data, 200)
	sma50 = calSMA(data, 50)
	# ema12 = calEMA(data, 12)
	# ema26 = calEMA(data, 26)
	# macd = np.where(np.array(ema26)==0, 0, ema12 - ema26)

	macd_primary, signal_primary = calMACD(data, 34, 5, 5)
	macd_secondary, signal_secondary = calMACD(data, 30, 3, 4)

	# timeframe week data ===============================================
	try:
		dfwk = pd.read_csv("data/" + key_week + ".csv", parse_dates=['Date'])
		dfwk.dropna(axis=0, how='any', inplace=True)#drop null row
		dfwk = dfwk[(dfwk != 0).all(1)]#drop 0 row
		dfwk = dfwk.drop_duplicates(subset=['Date'], keep='last')

		dwk = (dfwk.get(['Close']).values)*price_multiplier

		if(len(data) > len(dwk)):
			dwk = np.insert(dwk, 0, np.zeros((len(data) - len(dwk), 1)), axis=0)
		else:
			dwk = dwk[-len(data):]
		
	except Exception as e:
		print("Exception", e)
		dwk = data

	macd_primary_wk, signal_primary_wk = calMACD(dwk, 34, 5, 5)
	macd_secondary_wk, signal_secondary_wk = calMACD(dwk, 30, 3, 4)
	# end timeframe week data ============================================

	# feature = np.append(feature, np.reshape(sma200, (sma200.shape[0], 1)), axis=1)
	feature = np.append(feature, np.reshape(sma50, (sma50.shape[0], 1)), axis=1)

	feature = np.append(feature, np.reshape(signal_primary, (signal_primary.shape[0], 1)), axis=1)
	feature = np.append(feature, np.reshape(signal_secondary, (signal_secondary.shape[0], 1)), axis=1)
	feature = np.append(feature, np.reshape(signal_primary_wk, (signal_primary_wk.shape[0], 1)), axis=1)
	feature = np.append(feature, np.reshape(signal_secondary_wk, (signal_secondary_wk.shape[0], 1)), axis=1)

	macd_dict = {
		'macd_primary': signal_primary,
		'macd_secondary': signal_secondary, 
		'macd_primary_wk': signal_primary_wk,
		'macd_secondary_wk': signal_secondary_wk
	}

	return np.reshape(data, (data.shape[0], )) , feature, sma200, sma50, macd_dict

def calSMA(data, bar_count):

	l = len(data)

	smas = np.zeros(bar_count - 1)

	for i in range(bar_count - 1, l):

		sma = sum(data[i - bar_count + 1:i + 1])/bar_count

		smas = np.append(smas, sma)

	return smas

def calEMA(data, bar_count):

	l = len(data)

	emas = np.zeros(bar_count - 1)

	init_sma = sum(data[0:bar_count])/bar_count
	multiplier = 2/(bar_count+1)

	emas = np.append(emas, init_sma)

	for i in range(bar_count - 1 + 1, l):

		ema = (data[i] - emas[i-1])*multiplier+emas[i-1]
		emas = np.append(emas, ema)

	return emas

def calMACD(data, long_cycle_count, short_cycle_count, signal_periods_count):

	long_cycle = calEMA(data, long_cycle_count)
	short_cycle = calEMA(data, short_cycle_count)

	macd = np.where(np.array(long_cycle)==0, 0, short_cycle - long_cycle)

	signal = calEMA(macd, signal_periods_count)

	return macd, signal

def calTrendline(key, bar_count=0):

	line_high = []
	line_low = []

	# data = pd.read_csv("data/" + key + ".csv", parse_dates=['Date'], index_col='Date')
	data = pd.read_csv("data/" + key + ".csv")
	data.dropna(axis=0, how='any', inplace=True)#drop null row
	data = data[(data != 0).all(1)]#drop 0 row
	data = data.drop_duplicates(subset=['Date'], keep='last')
	data.reset_index(drop=True, inplace=True)

	data_len = len(data)

	if bar_count != 0:
		data = data[-(bar_count):]

	data0 = data.copy()
	# data0['date_id'] = ((data0.index.date - data0.index.date.min()))
	data0['date_id'] = ((data0.index - data0.index.min()))+1
	# data0['date_id'] = data0['date_id'].dt.days + 1

	data1 = data0.copy()

	while len(data1)>3:

		reg = linregress(
						x=data1['date_id'],
						y=data1['Close'],
						)

		data1 = data1.loc[data1['Close'] > reg[0] * data1['date_id'] + reg[1]]

	# reg = linregress(
	# 					x=data1['date_id'],
	# 					y=data1['High'],
	# 					)

	line_high = reg[0] * data0['date_id'] + reg[1]

	# # low trend line

	data1 = data0.copy()

	while len(data1)>3:

		reg = linregress(
						x=data1['date_id'],
						y=data1['Close'],
						)
		data1 = data1.loc[data1['Close'] < reg[0] * data1['date_id'] + reg[1]]

	# reg = linregress(
	# 					x=data1['date_id'],
	# 					y=data1['Low'],
	# 					)

	line_low = reg[0] * data0['date_id'] + reg[1]

	if bar_count != 0:
		
		zeros = np.zeros((data_len-bar_count) - 1)
		line_high = np.append(zeros, line_high)
		line_low = np.append(zeros, line_low)

		line_high = [float('nan') if x==0 else x for x in line_high]
		line_low = [float('nan') if x==0 else x for x in line_low]

		return line_high, line_low

	return line_high.values, line_low.values

# returns the sigmoid
def sigmoid(x):

	# return 1 / (1 + math.exp(-x))
	return 1 / (1 + np.exp(-x))

# returns an an n-day state representation ending at time t
def getState(data, t, n, gym):

	data = data.tolist()

	d = t - n + 1
	block = data[d:t + 1] if d >= 0 else -d * [data[0]] + data[0:t + 1] # pad with t0
	block = np.array(block)
	
	# maximum_drawdowns = np.full((block.shape[0],1), gym.maximum_drawdown)
	# block = np.append(block, maximum_drawdowns, axis=1)

	exposures = np.full((block.shape[0],1), gym.exposure)
	block = np.append(block, exposures, axis=1)
	
	res = []
	for i in range(n - 1):
		res.append(sigmoid(block[i + 1] - block[i])*gym.state_multiply)

	return np.array([res])
