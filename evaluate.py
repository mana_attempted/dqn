import os
os.environ["CUDA_VISIBLE_DEVICES"]="3"

import keras
from keras.models import load_model

from agent.agent import Agent
from functions import *
import sys

import matplotlib.pyplot as plt

from gym import Gym

import json
import numpy as np

category = "stock"
mode = "long"

if len(sys.argv) == 3:
	stock_name, model_name = sys.argv[1], sys.argv[2]	
elif len(sys.argv) == 4:
	stock_name, model_name, mode = sys.argv[1], sys.argv[2], sys.argv[3]
else:
	print("Usage: python evaluate.py [stock] [model]")
	exit()

model = load_model("models/" + model_name)
window_size = model.layers[0].input.shape.as_list()[1]

configs = json.load(open('config.json', 'r'))

price_multiplier = 1
if category == "forex":
	price_multiplier = 1000

agent = Agent(window_size, True, model_name)
data, feature, sma200, sma50, macd_dict = getStockDataVec(stock_name, str(stock_name)+"_week", configs['data']['columns'], price_multiplier)
line_high, line_low = calTrendline(stock_name)
line_high50, line_low50 = calTrendline(stock_name, 50)
line_high100, line_low100 = calTrendline(stock_name, 100)
line_high150, line_low150 = calTrendline(stock_name, 150)

l = len(data) - 1
batch_size = agent.batch_size

gym = Gym(category, mode)

def plotGraph(price, buy_actions, sell_actions, model, profit, count_trade):

	f, (a0, a1, a2) = plt.subplots(3, 1, sharex=True, gridspec_kw={'height_ratios':[2, 1, 1]}, figsize=(24, 14))

	a0.plot(price, label='price')

	if mode == 'short':
		a0.plot(buy_actions, "r.", label='open short')
		a0.plot(sell_actions, "g.", label='close short')
	else:
		a0.plot(buy_actions, "g.", label='buy')
		a0.plot(sell_actions, "r.", label='sell')

	a0.plot([float('nan') if x==0 else x for x in sma50], label="SMA50")

	a0.set_title(stock_name+" "+model + " profit: "+ profit)

	a0.plot(line_high, label='upper trendline')
	a0.plot(line_low, label='bottom trendline')
	a0.plot(line_high50, label='50 upper trendline')
	a0.plot(line_low50, label='50 bottom trendline')
	a0.plot(line_high100, label='100 upper trendline')
	a0.plot(line_low100, label='100 bottom trendline')
	a0.plot(line_high150, label='150 upper trendline')
	a0.plot(line_low150, label='150 bottom trendline')

	a0.grid(ls='--')
	a0.legend(loc='upper left', frameon=True)

	# a1.plot([float('nan') if x==0 else x for x in ema12], label="EMA12")
	# a1.plot([float('nan') if x==0 else x for x in ema26], label="EMA26")

	a1.set_title("MACD day")
	a1.plot([float('nan') if x==0 else x for x in macd_dict['macd_primary']], label="MACD(34,5,5) signal")
	a1.plot([float('nan') if x==0 else x for x in macd_dict['macd_secondary']], label="MACD(30,3,4) signal")
	a1.plot(np.zeros(len(price)), label="VLINE")
	a1.grid(ls='--')
	a1.legend(loc='upper left', frameon=True)

	a2.set_title("MACD week")
	a2.plot([float('nan') if x==0 else x for x in macd_dict['macd_primary_wk']], label="MACD(34,5,5) signal")
	a2.plot([float('nan') if x==0 else x for x in macd_dict['macd_secondary_wk']], label="MACD(30,3,4) signal")
	a2.plot(np.zeros(len(price)), label="VLINE")
	a2.grid(ls='--')
	a2.legend(loc='upper left', frameon=True)
	
	# f.show()
	# f.tight_layout()
	
	f.savefig("images/"+stock_name+""+model+".png", format='png', bbox_inches='tight', transparent=True)

state = getState(feature, 0, window_size + 1, gym)

total_profit = 0

buy_actions = []
sell_actions = []

count_buy = 0
count_sell = 0
count_trade = 0
count_sit = 0

factor_hold = 1.00

def clearPosition(action_bar, action_price, total_profit):

	while len(gym.inventory) > 0:

		reward, total_profit, percent, holding_period, port = gym.sell(action_bar, action_price, total_profit)

		gym.collectStat(reward, percent, holding_period, port)

	return  total_profit

for t in range(l):

	action = agent.act(state)

	# sit
	next_state = getState(feature, t + 1, window_size + 1, gym)
	reward = 0

	if action == 0 and len(gym.inventory) > 0: # hold

		factor_hold += 0.01

	if action == 1: # buy
	# if action == 1 and len(gym.inventory) < 1: # buy

		port = gym.buy(t, data[t])

		action_price = data[t]*gym.position_size

		if(gym.cash >= action_price):

			buy_actions.append(data[t])
			sell_actions.append(None)

			count_buy+=1
			count_trade+=1
		else:
			buy_actions.append(None)
			sell_actions.append(None)

	elif action == 2 and len(gym.inventory) > 0: # sell

		reward, total_profit, percent, holding_period, port = gym.sell(t, data[t], total_profit)

		factor_hold = 1.00

		gym.collectStat(reward, percent, holding_period, port)

		buy_actions.append(None)
		sell_actions.append(data[t])

		count_sell+=1
		count_trade+=1

	else:
		buy_actions.append(None)
		sell_actions.append(None)

	done = True if t == l - 1 else False

	reward = gym.calReward(reward, factor_hold, macd_dict, t, action)

	agent.memory.append((state, action, reward, next_state, done))
	state = next_state

	if t % 244 == 0: #annual collect

		gym.collectAnnual()

	if done:

		total_profit = clearPosition(t, data[t], total_profit)

		gym.calculateSharpeRaio()

		gym.printStat()
		
	# if len(agent.memory) > batch_size:
	# 	agent.expReplay(batch_size)

plotGraph(data, buy_actions, sell_actions, model_name, formatPrice(total_profit), count_trade)