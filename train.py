import os
os.environ["CUDA_VISIBLE_DEVICES"]="1"

from agent.agent import Agent
from gym import Gym
from functions import *
import sys
import matplotlib.pyplot as plt
import json

# if len(sys.argv) != 4:
# 	print("Usage: python train.py [stock] [window] [episodes]")
# 	exit()

is_eval = False
model_name = ""
from_episode = 0

category = "stock"
mode = "long"

if len(sys.argv) == 4:
	stock_name, window_size, episode_count = sys.argv[1], int(sys.argv[2]), int(sys.argv[3])	
elif len(sys.argv) == 5:
	stock_name, window_size, episode_count, mode = sys.argv[1], int(sys.argv[2]), int(sys.argv[3]), sys.argv[4]
elif len(sys.argv) == 7:
	stock_name, window_size, episode_count, is_eval, model_name, from_episode = sys.argv[1], int(sys.argv[2]), int(sys.argv[3]), sys.argv[4] == "True", sys.argv[5], int(sys.argv[6])
else:
	print("Usage: python train.py [stock] [window] [episodes] [is_eval] [model_name] [from_episode]")
	exit()

configs = json.load(open('config.json', 'r'))

agent = Agent(window_size, is_eval, model_name)
# data = getStockDataVec(stock_name)
# data, feature = getStockDataVec(stock_name, configs['data']['columns'])
# l = len(data) - 1
batch_size = agent.batch_size

gym = Gym(category, mode)

price_multiplier = 1
if category == "forex":
	price_multiplier = 1000

# gym.prices = data

def plotGraph(profits):

	fig = plt.figure(figsize=(14,7), facecolor='white')
	ax = fig.add_subplot(111)
	ax.plot(profits, label='profit')
	# ax.plot(trades, label='count trade')
	plt.title(stock_name + " profit each episode" + str(episode_count+from_episode) + " ep "+str(window_size)+"window.png")
	plt.grid(ls='--')
	plt.legend(loc='upper left', frameon=True)
	# plt.show()
	plt.savefig("images/"+stock_name+"profit"+str(episode_count+from_episode)+"ep"+str(window_size)+"window.png", format='png', bbox_inches='tight', transparent=True)
	plt.cla()
	plt.close(fig)

prev_total_profit = -1000000

for e in range(episode_count + 1):

	data, feature, sma200, sma50, macd_dict = getStockDataVec("slippage/"+str(stock_name)+"/"+str(e+1), "slippage/"+str(stock_name)+"_week/"+str(e+1), configs['data']['columns'], price_multiplier)

	# smas = [float('nan') if x==0 else x for x in smas]
	# plt.plot(smas200, label='SMA200')
	# plt.plot(data, label='Close')
	# plt.legend(loc='upper left', frameon=True)
	# plt.show()
	# exit()

	l = len(data) - 1
	gym.prices = data
	
	state = getState(feature, 0, window_size + 1, gym)

	total_profit = 0

	factor_hold = 1.00
	factor_holds = []

	gym.clearStat()

	for t in range(l):

		# action = agent.actTrain(state, data[t], sma50[t])
		action = agent.act(state)

		# sit
		next_state = getState(feature, t + 1, window_size + 1, gym)
		reward = 0

		if action == 0 and len(gym.inventory) > 0: # hold

			factor_hold += 0.01
		
		if action == 1: # buy
		# if action == 1 and len(gym.inventory) < 1: # buy

			port = gym.buy(t, data[t])

		elif action == 2 and len(gym.inventory) > 0: # sell

			reward, total_profit, percent, holding_period, port = gym.sell(t, data[t], total_profit)

			factor_holds.append(factor_hold)

			factor_hold = 1.00

			gym.collectStat(reward, percent, holding_period, port)

		done = True if t == l - 1 else False

		reward = gym.calReward(reward, factor_hold, macd_dict, t, action)

		agent.memory.append((state, action, reward, next_state, done))
		state = next_state

		if t % 244 == 0: #annual collect

			gym.collectAnnual()

		if done:
			print ("Episode " + str(e) + "/" + str(episode_count)+" Total Profit: " + formatPrice(total_profit)+"/"+formatPrice(prev_total_profit)+ " epsilon: "+ formatPrice(agent.epsilon))
			# print("max(factor_holds)", max(factor_holds))

			gym.calculateSharpeRaio()
			
			gym.printStat()

			gym.profits.append(total_profit)

		if len(agent.memory) > batch_size:

			agent.expReplay(batch_size)

	if e % 100 == 0:
		plotGraph(gym.profits)
	if e % 500 == 0:
		agent.model.save("models/" + stock_name + "ep" + str(e+from_episode) + "window" + str(window_size))
	if total_profit > prev_total_profit:
		prev_total_profit = total_profit
		agent.model.save("models/" + stock_name + "ep" + str(e+from_episode) + "window" + str(window_size))

plotGraph(gym.profits)
