from functions import *
from statistics import mean, stdev
import math

INIT_CASH = 100000
POSITION_SIZE = 100

MODEL_SCORE_TABLE = {
	'1111': {
		1: 15,
		2: 0
	},
	'1110': {
		1: 14,
		2: 1
	},
	'1101': {
		1: 13,
		2: 2
	},
	'1100': {
		1: 12,
		2: 3
	},
	'1011': {
		1: 11,
		2: 4
	},
	'1010': {
		1: 10,
		2: 5
	},
	'1001': {
		1: 9,
		2: 6
	},
	'1000': {
		1: 8,
		2: 7
	},
	'0111': {
		1: 7,
		2: 8
	},
	'0110': {
		1: 6,
		2: 9
	},
	'0101': {
		1: 5,
		2: 10
	},
	'0100': {
		1: 4,
		2: 11
	},
	'0011': {
		1: 3,
		2: 12
	},
	'0010': {
		1: 2,
		2: 13
	},
	'0001': {
		1: 1,
		2: 14
	},
	'0000': {
		1: 0,
		2: 15
	}
}

class Gym:
	def __init__(self, category="stock", mode="long"):
		self.category = category
		self.mode = mode
		self.profits = []
		self.trades = []
		self.maximum_drawdowns = []
		self.maximum_profits = []

		self.leverage = 1
		self.state_multiply = 1
		if category == "forex":
			self.leverage = 100
			self.state_multiply = 10000

		self.prices = []

		self.clearStat()

	def buy(self, action_bar, action_price):

		self.buy_price = action_price

		action_price*=self.position_size

		if(self.cash >= action_price):
			
			self.inventory.append(Transaction(action_bar, action_price))

			self.cash -= action_price
			self.stock += action_price

		port = self.calPort()

		return port

	def sell(self, action_bar, action_price, total_profit):

		action_price*=self.position_size
		transaction = self.inventory.pop(0)

		bought_bar = transaction.action_bar
		bought_price = transaction.action_price

		opposite = 1
		if self.mode == "short":
			opposite = -1

		reward = (action_price - bought_price)*opposite

		total_profit += (action_price - bought_price)*opposite
		
		percent = (reward/bought_price)*100
		holding_period = action_bar - bought_bar

		self.cash += action_price
		self.stock -= bought_price

		port = self.calPort()

		self.collectLiveStat(bought_bar, bought_price, action_bar, action_price)

		return reward, total_profit, percent, holding_period, port

	def calReward(self, originalReward, factor_hold, macd_dict, action_bar, action):

		str_code = ""

		is_macd_signal_primary_above_week = False
		is_macd_signal_primary_above = False

		if(macd_dict['macd_primary_wk'][action_bar] > macd_dict['macd_secondary_wk'][action_bar]):
			is_macd_signal_primary_above_week = True

		if(macd_dict['macd_primary'][action_bar] > macd_dict['macd_secondary'][action_bar]):
			is_macd_signal_primary_above = True

		if(self.is_macd_signal_primary_above_week != is_macd_signal_primary_above_week):
			str_code += "1"
		else:
			str_code += "0"

		if(is_macd_signal_primary_above_week > 0):
			str_code += "1"
		else:
			str_code += "0"

		if(self.is_macd_signal_primary_above != is_macd_signal_primary_above):
			str_code += "1"
		else:
			str_code += "0"

		if(is_macd_signal_primary_above > 0):
			str_code += "1"
		else:
			str_code += "0"

		self.is_macd_signal_primary_above_week = is_macd_signal_primary_above_week
		self.is_macd_signal_primary_above = is_macd_signal_primary_above

		macd_factor = 0

		if(action == 0):
			macd_factor = 0
		else:
			macd_factor = MODEL_SCORE_TABLE[str_code][action]	
			if self.mode == "short":
				macd_factor = 15 - macd_factor

		macd_factor = 1+(macd_factor/100)
		
		if(macd_factor == 0):
			print("macd_factor = 0")
			exit()

		return originalReward * factor_hold * macd_factor

	def isGain(self, profit):
		return profit > 0

	def isLoss(self, profit):
		return profit < 0

	def calChange(self, considering_round, total_round):

		if(total_round == 0):
			return 0

		if((considering_round/total_round)*100 == 100 and considering_round != total_round):
			print(str(considering_round)+"/"+str(total_round)+"="+str((considering_round/total_round)*100))

		return (considering_round/total_round)*100
	
	def calAverage(self, total, quantity):

		if(quantity == 0):
			return 0
		return total/quantity

	def calExpectancy(self):

		return (self.winning_change*self.average_percent_gain)+(self.losing_change*self.average_percent_loss)

	def collectStat(self, result, percent, holding_period, port):

		self.total_round+=1

		if port > self.port_peak:
			self.port_peak = port
		elif port < self.port_low:
			self.port_low = port	

		if self.isGain(result):
			self.gains.append(result)
			self.gain_round+=1
			self.percent_gains.append(percent)
			self.total_gain+=result
			self.total_gain_percent = calPercent(INIT_CASH, self.total_gain)
			self.holding_gains.append(holding_period)
			self.consecutive_win+=1
			self.consecutive_losses.append(self.consecutive_loss)
			self.consecutive_loss = 0

		elif self.isLoss(result):
			self.losses.append(result)
			self.loss_round+=1
			self.percent_losses.append(percent)
			self.total_loss+=result
			self.total_loss_percent = calPercent(INIT_CASH, self.total_loss)
			self.holding_losses.append(holding_period)
			self.consecutive_loss+=1
			self.consecutive_wins.append(self.consecutive_win)
			self.consecutive_win = 0

			drawdown = self.port_peak - self.port_low
			self.drawdowns.append(drawdown)
			self.maximum_drawdown = max(self.drawdowns)
			self.maximum_drawdown_percent = calPercent(self.port_peak, self.maximum_drawdown)
			self.average_drawdown = mean(self.drawdowns) if len(self.drawdowns) > 0 else 0
			self.average_drawdown_percent = calPercent(self.port_peak, self.average_drawdown)

		self.total_net_gain+=result
		self.total_net_gain_percent = calPercent(INIT_CASH, self.total_net_gain)

		self.holding_periods.append(holding_period)

		self.winning_change = self.calChange(self.gain_round, self.total_round)

		self.average_gain = mean(self.gains) if len(self.gains) > 0 else 0
		self.min_gain = min(self.gains) if len(self.gains) > 0 else 0
		self.max_gain = max(self.gains) if len(self.gains) > 0 else 0
		self.average_percent_gain = mean(self.percent_gains) if len(self.percent_gains) > 0 else 0
		self.min_percent_gain = min(self.percent_gains) if len(self.percent_gains) > 0 else 0
		self.max_percent_gain = max(self.percent_gains) if len(self.percent_gains) > 0 else 0

		self.average_loss = mean(self.losses) if len(self.losses) > 0 else 0
		self.min_loss = max(self.losses) if len(self.losses) > 0 else 0
		self.max_loss = min(self.losses) if len(self.losses) > 0 else 0
		self.average_percent_loss = mean(self.percent_losses) if len(self.percent_losses) > 0 else 0
		self.min_percent_loss = max(self.percent_losses) if len(self.percent_losses) > 0 else 0
		self.max_percent_loss = min(self.percent_losses) if len(self.percent_losses) > 0 else 0

		self.losing_change = self.calChange(self.loss_round, self.total_round)
		self.expectancy = self.calExpectancy()
		self.average_holding_gain = mean(self.holding_gains) if len(self.holding_gains) > 0 else 0
		self.max_holding_gain = max(self.holding_gains) if len(self.holding_gains) > 0 else 0
		self.min_holding_gain = min(self.holding_gains) if len(self.holding_gains) > 0 else 0
		self.average_holding_loss = mean(self.holding_losses) if len(self.holding_losses) > 0 else 0
		self.max_holding_loss = max(self.holding_losses) if len(self.holding_losses) > 0 else 0
		self.min_holding_loss = min(self.holding_losses) if len(self.holding_losses) > 0 else 0
		self.average_holding_period = mean(self.holding_periods) if len(self.holding_periods) > 0 else 0
		self.max_holding_period = max(self.holding_periods) if len(self.holding_periods) > 0 else 0
		self.min_holding_period = min(self.holding_periods) if len(self.holding_periods) > 0 else 0

		self.maximum_consecutive_win = max(self.consecutive_wins) if len(self.consecutive_wins) > 0 else 0
		self.maximum_consecutive_loss = max(self.consecutive_losses) if len(self.consecutive_losses) > 0 else 0

		self.profit_factor = (self.gain_round * self.average_gain) / (self.loss_round * self.average_loss) if self.loss_round > 0 else self.gain_round * self.average_gain

		self.calculateSharpeRaio()

		self.trade_returns.append(percent)

	def collectLiveStat(self, buy_bar, buy_price, sell_bar, sell_price):

		holding_price = self.prices[buy_bar:sell_bar+1]

		self.holding_price_high = max(holding_price) if len(holding_price) > 0 else 0
		self.holding_price_low = min(holding_price) if len(holding_price) > 0 else 0

		self.mfe = self.holding_price_high - self.buy_price
		self.mae = self.buy_price - self.holding_price_low
		mfe_by_mae = self.mfe/self.mae if self.mfe != 0 and self.mae != 0 else 0
		self.mfe_by_maes.append(mfe_by_mae)
		self.average_mfe_by_mae = mean(self.mfe_by_maes)
		
	def collectAnnual(self):

		MULTIPLY = 244

		aar = self.total_net_gain - sum(self.aars)

		self.aars.append(aar)
		self.average_aar = mean(self.aars)
		self.aar_by_mdd = self.average_aar/self.maximum_drawdown if self.maximum_drawdown > 0 else 0

		annualized_expected_return = (mean(self.trade_returns))*MULTIPLY if len(self.trade_returns) > 0 else 0
		self.annualized_expected_returns.append(annualized_expected_return)

		variance = (stdev(self.trade_returns))*MULTIPLY if len(self.trade_returns) > 2 else 0
		std = math.sqrt(variance)
		self.stds.append(std)

		self.trade_returns = []

	def clearStat(self):

		self.total_round = 0

		self.gains = []
		self.gain_round = 0
		self.winning_change = 0.00
		self.percent_gains = []
		self.average_gain = 0
		self.min_gain = 0
		self.max_gain = 0
		self.average_percent_gain = 0.00
		self.min_percent_gain = 0.00
		self.max_percent_gain = 0.00
		self.total_gain = 0
		self.total_gain_percent = 0
		self.holding_gains = []
		self.average_holding_gain = 0
		self.max_holding_gain = 0
		self.min_holding_gain = 0

		self.losses = []
		self.loss_round = 0
		self.losing_change = 0.00
		self.percent_losses = []
		self.average_loss = 0
		self.min_loss = 0
		self.max_loss = 0
		self.average_percent_loss = 0.00
		self.min_percent_loss = 0.00
		self.max_percent_loss = 0.00
		self.total_loss = 0
		self.total_loss_percent = 0
		self.holding_losses = []
		self.average_holding_loss = 0
		self.max_holding_loss = 0
		self.min_holding_loss = 0

		self.expectancy = 0.00
		self.total_net_gain = 0
		self.total_net_gain_percent = 0
		self.holding_periods = []
		self.average_holding_period = 0
		self.max_holding_period = 0
		self.min_holding_period = 0

		self.inventory = []

		self.port = 0
		self.cash = INIT_CASH
		self.cash_percent = 0
		self.stock = 0
		self.stock_percent = 0

		self.position_size = POSITION_SIZE/self.leverage

		self.port_peak = INIT_CASH
		self.port_low = INIT_CASH

		self.drawdowns = []
		self.maximum_drawdown = 0
		self.maximum_drawdown_percent = 0
		self.average_drawdown = 0
		self.average_drawdown_percent = 0

		self.consecutive_win = 0
		self.consecutive_wins = []
		self.consecutive_loss = 0
		self.consecutive_losses = []
		self.maximum_consecutive_win = 0
		self.maximum_consecutive_loss = 0

		self.profit_factor = 0

		self.buy_price = 0
		self.holding_price_high = 0
		self.holding_price_low = 0
		self.mfe = 0
		self.mae = 0
		self.mfe_by_maes = []
		self.average_mfe_by_mae = 0

		self.sharpe_ratio = 0

		self.aars = []
		self.average_aar = 0
		self.aar_by_mdd = 0

		self.exposure = 0
		
		self.trade_returns = []
		self.annualized_expected_returns = []
		self.stds = []

		self.is_macd_signal_primary_above = True
		self.is_macd_signal_primary_above_week = True

		self.calPort()

	def calPort(self):

		self.cash = max(self.cash, 0)
		self.stock = max(self.stock, 0)
		
		port = self.cash + self.stock

		self.port = port

		self.exposure = (self.stock/self.port)*100

		self.cash_percent = calPercent(self.port, self.cash)
		self.stock_percent = calPercent(self.port, self.stock)

		return port

	def restartPort(self):

		self.port = 0
		self.cash = INIT_CASH
		self.stock = 0
		self.calPort()

	def printStat(self):
		print("================================================GYM STAT================================================")
		print("port start: "+ formatPrice(INIT_CASH))
		print("total_round: "+str(self.total_round)+"\t\t\t gain_round: "+str(self.gain_round)+"\t\t\t loss round: "+str(self.loss_round))
		print("\t\t\t\t\t winning_change: "+formatPercent(self.winning_change)+"\t\t\t losing_change: "+formatPercent(self.losing_change))
		print("average_gain: "+formatPrice(self.average_gain)+"("+formatPercent(self.average_percent_gain)+")\t\t max_gain: "+formatPrice(self.max_gain)+"("+formatPercent(self.max_percent_gain)+")\t\t min_gain: "+formatPrice(self.min_gain)+"("+formatPercent(self.min_percent_gain)+")")
		print("average_loss: "+formatPrice(self.average_loss)+"("+formatPercent(self.average_percent_loss)+")\t\t max_loss: "+formatPrice(self.max_loss)+"("+formatPercent(self.max_percent_loss)+")\t\t min_loss: "+formatPrice(self.min_loss)+"("+formatPercent(self.min_percent_loss)+")")
		print("expectancy: "+formatPercent(self.expectancy)+"\t\t\t total_gain: "+formatPrice(self.total_gain)+"("+formatPercent(self.total_gain_percent)+")\t\t total_loss: "+ formatPrice(self.total_loss)+"("+formatPercent(self.total_loss_percent)+")")
		print("total_net_gain: "+formatPrice(self.total_net_gain)+"("+formatPercent(self.total_net_gain_percent)+")")
		print("average_holding_period: "+formatInteger(self.average_holding_period)+"\t\t average_holding_gain: "+formatInteger(self.average_holding_gain)+"\t\t average_holding_loss: "+ formatInteger(self.average_holding_loss))
		print("max_holding_period: "+formatInteger(self.max_holding_period)+"\t\t\t max_holding_gain: "+formatInteger(self.max_holding_gain)+"\t\t\t max_holding_loss: "+ formatInteger(self.max_holding_loss))
		print("min_holding_period: "+formatInteger(self.min_holding_period)+"\t\t\t min_holding_gain: "+formatInteger(self.min_holding_gain)+"\t\t\t min_holding_loss: "+ formatInteger(self.min_holding_loss))
		print("maximum_drawdown: "+formatPrice(self.maximum_drawdown)+"("+formatPercent(self.maximum_drawdown_percent)+")"+"\t average_drawdown : "+formatPrice(self.average_drawdown)+"("+formatPercent(self.average_drawdown_percent)+")")
		print("maximum_consecutive_win: "+formatInteger(self.maximum_consecutive_win)+"\t\t maximum_consecutive_loss : "+formatInteger(self.maximum_consecutive_loss)+"\t\t AAR/MDD : "+formatPrice(self.aar_by_mdd))
		print("profit_factor: "+formatPrice(self.profit_factor)+"\t\t\t avg MFE/MAE: "+formatPrice(self.average_mfe_by_mae)+"\t\t\t sharpe_ratio: "+formatPrice(self.sharpe_ratio))
		print("cash: "+formatPrice(self.cash)+"("+formatPercent(self.cash_percent)+")"+"\t\t\t\t stock: "+formatPrice(self.stock)+"("+formatPercent(self.stock_percent)+")")
		print("port: "+formatPrice(self.port)+"\t\t\t\t exposure: "+formatPercent(self.exposure))
		print("\n")

	def calculateSharpeRaio(self):

		expected_return = mean(self.annualized_expected_returns) if len(self.annualized_expected_returns) > 0 else 0
		risk_free_rate = 3.02
		standard_deviation = mean(self.stds) if len(self.stds) > 0 else 0

		self.sharpe_ratio = (expected_return - risk_free_rate)/standard_deviation if standard_deviation > 0 else 0

class Transaction :
	def __init__(self, action_bar=0, action_price=0):
		self.action_bar = action_bar
		self.action_price = action_price